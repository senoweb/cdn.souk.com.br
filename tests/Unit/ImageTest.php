<?php

namespace Tests\Unit;

use App\Entities\Image;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ImageTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testImageUpload()
    {
        Storage::fake('public');

        $file = UploadedFile::fake()->image('avatar.jpg');

        $response = $this->json('POST', '/api/image', [
            'image' => $file,
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'id' => Image::first()->id,
                    'url' => Image::first()->url,
                ]
            ]);

        Storage::disk('public')->assertExists(Image::first()->file);

        Storage::disk('public')->assertMissing('avatar.jpg');
    }

    public function testUpdateImageUpload()
    {
        Storage::fake('public');

        $file = UploadedFile::fake()->image('avatar.jpg');

        $image = $this->json('POST', '/api/image', [
            'image' => $file,
        ])->decodeResponseJson();

        $response = $this->json('POST', '/api/image/'.$image['data']['id'], [
            'image' => $file,
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => Image::first()->id,
                    'url' => Image::first()->url,
                ]
            ]);

        Storage::disk('public')->assertExists(Image::first()->file);
    }

    public function testImageUrl()
    {
        Storage::fake('public');

        $file = UploadedFile::fake()->image('avatar.jpg');

        $image = $this->json('POST', '/api/image', [
            'image' => $file,
        ])->decodeResponseJson();

        $response = $this->json('GET', '/api/image/'.$image['data']['id']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => Image::first()->id,
                    'url' => Storage::disk('public')->url(Image::first()->file),
                ]
            ]);
    }
}
