CDN Souk
==========

This is the API repository for the Souk MVC Project, developed with ❤️ and ☕ 

----

Requirements:

- All the [Laravel requirements](https://laravel.com/docs/5.6/installation)
- MySQL Server
- PhpUnit

----

change to root folder
```
$ cd cdn.souk.com.br
```

Install the dependencies
```
$ composer install
```

Copy .env.example to .env
```
$ cp .env.example .env
```

Generate the app key
```
$ php artisan key:generate
```

Grant framework storage permission
```
$ chgrp -R www-data storage bootstrap/cache
$ chmod -R ug+rwx storage bootstrap/cache
```

Create database and run migrations
```
$ create database cdn_souk;
$ create database cdn_souk_testing;
$ php artisan migrate
```

Run Tests
```
$ vendor/bin/phpunit
```

Start serve
```
$ php artisan serve --port=8001
```

CDN API data structure
==
base_uri: /api

Application
--

- **POST /image**
Param: image
Return:
200
```
{
	"id": "5",
	"url": "http://localhost:8001/storage/images/MtMjZmBawIW3EidT2KVky7nhr44fNAds.png"
}
```

- **GET /image/{id}**
Return:
200
```
{
	"id": "5",
	"url": "http://localhost:8001/storage/images/MtMjZmBawIW3EidT2KVky7nhr44fNAds.png"
}
```

- **POST /image/{id}**
Param: id
Param: image
Return:
200
```
{
	"id": "5",
	"url": "http://localhost:8001/storage/images/MtMjZmBawIW3EidT2KVky7nhr44fNAds.png"
}
```

4xx:
```
{
    "error": true,
    "code": 0,
    "message": {
        "image": [
            "The image field is required."
        ]
    }
}
```

4xx:
**Debug mode on**
```
{
    "error": true,
    "code": 0,
    "message": {
        "image": [
            "The image field is required."
        ]
    },
    "stack_trace": ""
}
```