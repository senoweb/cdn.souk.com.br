<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'file'
    ];

    protected $appends = [
        'url'
    ];

    public function getUrlAttribute() : string
    {
        return Storage::disk('public')->url($this->file);
    }
}
