<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class DBTransaction
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure                 $next
   *
   * @return mixed
   * @throws \Exception
   */
  public function handle($request, Closure $next)
  {

    \DB::beginTransaction();

    $request = $next($request);

    \DB::commit();

    return $request;
  }
}
