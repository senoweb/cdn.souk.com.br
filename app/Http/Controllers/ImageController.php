<?php

namespace App\Http\Controllers;

use App\Entities\Image;
use App\Http\Requests\storeImageRequest;
use App\Http\Resources\ImageResource;
use App\Services\ImageService;
use Illuminate\Http\Request;

class ImageController
 extends Controller
{
    protected $service;

    function __construct(ImageService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Image $image)
    {
        return new ImageResource($image);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(storeImageRequest $request)
    {
        $image = $this->service->storeImage($request->file('image'));

        return new ImageResource($image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(storeImageRequest $request, Image $image)
    {
        $this->service->updateImage($image, $request->file('image'));

        return new ImageResource($image);
    }
}
