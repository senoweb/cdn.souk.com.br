<?php

namespace App\Services;

use App\Entities\Image;
use App\Exceptions\StandardError;
use App\Helpers\RandomToken;
use Exception;
use Illuminate\Support\Facades\Storage;

class ImageService extends Service
{
    public function __construct()
    {
        parent::__construct();
    }

    public function storeImage($file) : Image
    {
        try {
            $name = RandomToken::generate() . '.' . $file->getClientOriginalExtension();
            $store = $file->storeAs('images', $name);

            $image = Image::create([
                'name' => $name,
                'file' => $store,
            ]);

            return $image;
        } catch (Exception $e) {
            throw StandardError::raise('image', $e->getMessage(), 400, null);
        }
    }

    public function updateImage(Image $image, $file) : Image
    {
        try {
            $store = $file->storeAs('images', $image->name);

            $image->file = $store;

            if(!$image->save())
                $this->raiseUpdateImage();

            return $image;
        } catch (Exception $e) {
            throw StandardError::raise('image', $e->getMessage(), 400, null);
        }
    }

    protected function raiseUpdateImage() : void
    {
        throw StandardError::raise('image', trans('image.failed_update'), 400, null);
    }
}
