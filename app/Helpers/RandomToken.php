<?php

namespace App\Helpers;

class RandomToken {

    public static function generate($minSize = 32, $maxSize = 32, $specialChars = false) {

        // The token size is randomly between 32 and $maxSize:
        if($maxSize < 32) {
            $sz = 32;
        } else {
            $sz = $maxSize;
        }

        $size = random_int($minSize, $sz);

        if($specialChars)
        {
            $validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~`!@#$%^&*()-_=+{}[]|/?<>,.';
        } else {
            $validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        }

        $charBounds = strlen($validChars) - 1;

        $retval = '';

        // Loop through the list of chars and generate the new token:
        for($i = 0; $i < $size; $i++) {
            $pos = random_int(0, $charBounds);
            $retval .= $validChars[$pos];
        }

        return $retval;
    }

    public static function generateNumeric($maxSize = 6) {

        if($maxSize < 6) {
            $sz = 6;
        } else {
            $sz = $maxSize;
        }

        $size = random_int(6, $sz);

        $validChars = '0123456789';

        $charBounds = strlen($validChars) - 1;

        $retval = '';

        // Loop through the list of chars and generate the new token:
        for($i = 0; $i < $size; $i++) {
            $pos = random_int(0, $charBounds);
            $retval .= $validChars[$pos];
        }

        return $retval;
    }
}
