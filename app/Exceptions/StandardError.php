<?php

namespace App\Exceptions;

/**
 *  Class representing the generic error in the system.
 *
 *  The architecture uses the concept of Business-Rule Stacking:
 *
 *  - To be used in the controllers, and then exposed to the world, the business
 *    class must be registered as a Service, through a Provider.
 *
 *  - A business class can call another business class directly, recursively.
 *
 *  - The Happy path is expected in the normal return of each business rule method.
 *
 *  - The Alternative paths are activated raising exceptions.
 *
 *  - It's expected the an alternative path interrupts the stack completely, and
 *    it should reach the controller.
 *
 *  - The controller must know how to deal with the raised exceptions.
 *    Otherwise, it must pack suitably the exception and throw it up to the
 *    caller.
 *
 *  - Interruptions in the stack of code, through exception handling can be
 *    catched, but only as in very "exceptional" cases. In such cases, if the
 *    exception will be re-raised, the $previous parameter in the exception must
 *    be attribued correctly.
 */

use Exception;

class StandardError extends Exception
{
    public $response;
    protected $httpCode;

    /**
     * The constructor. The Exception $code belongs to a static list of errors
     * which is standardized (Se the entire list of errors below). The $httpCode
     * parameter is compulsory and says which HTTP error code the controller
     * will return to the caller, if the exception reaches the controller, and
     * the $message parameter must be used to describe the error.
     *
     * The $previous parameter is the only optional, and must be used if an
     * exception has been catched and reevaluated.
     */
    public function __construct($message, int $httpCode, int $code, $previous)
    {
        parent::__construct('', $code);

        $this->httpCode = $httpCode;
        $this->message = $message;
    }

    /**
     * Get the http code from exception
     *
     * @return void
     */
    public function getHttpCode()
    {
        return $this->httpCode;
    }

    public function getMessageBag()
    {
        return $this->message;
    }

    /**
     * Transforms the error entity to a processable object
     *
     * @return array $message
     */
    public function toObj()
    {
        $message = [
            'error' => true,
            'code' => $this->getCode(),
            'message' => $this->getMessageBag(),
        ];


        if(config('app.debug') && (config('app.env') == 'local'))
        {
            $stackTrace = ['stack_trace' => $this->getTraceAsString()];
            $message = array_merge($message, $stackTrace);
        }

        return $message;
    }

    /**
     * PHP has a pristine stack trace management. So, all errors can be
     * created here, and called on the correct point of code.
     *
     * In this way, we can control all kinds of errors the system generate,
     * and we can manage them in a single file - this.
     *
     * The error codes are incremental.
     */
    public static function raise(string $exceptionName, $message, int $httpCode, $previous = null)
    {
        return self::$exceptionName($message, $httpCode, $previous = null);
    }

    /****
     ***
     *** The list of ALL known errors on system are below - all generated using
     *** the raise static function.
     ***
     *** PHP has a pristine stack trace management. So, all errors can be
     *** created here, and called on the correct point of code.
     ***
     *** In this way, we can control all kinds of errors the system generate,
     *** and we can manage them in a single file - this.
     ***
     *** The error codes are incremental.
     ***/

    /**
     * 0: System Error: internal server error. Generally 5xx.
     */
    protected static function system($message, int $httpCode = 500, $previous = null)
    {
        return new StandardError($message, $httpCode, 0, $previous);
    }

    protected static function image($message, int $httpCode = 400, $previous = null)
    {
        return new StandardError($message, $httpCode, 1, $previous);
    }
}
